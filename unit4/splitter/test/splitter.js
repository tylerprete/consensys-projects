var Splitter = artifacts.require("./Splitter.sol");

contract('Splitter', function(accounts) {
  it("should create a Splitter", function() {
    return Splitter.deployed().then(function(instance) {
      return 10000;
    }).then(function(balance) {
      assert.equal(balance.valueOf(), 10000, "10000 wasn't in the first account");
    });
  });
});