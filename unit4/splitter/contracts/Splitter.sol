/*

SPLITTER

You will create a smart contract named Splitter whereby:

there are 3 people: Alice, Bob and Carol
we can see the balance of the Splitter contract on the web page
whenever Alice sends ether to the contract,
half of it goes to Bob and the other half to Carol
we can see the balances of Alice, Bob and Carol on the web page
we can send ether to it from the web page
It would be even better if you could team up with different people
impersonating Alice, Bob and Carol, all cooperating on a test net.

Stretch goals:

add a kill switch to the whole contract
make the contract a utility that can be used by David, Emma and anybody
with an address cover potentially bad input data
 */

pragma solidity ^0.4.6;

contract Splitter is Owned {

    mapping(address => uint) balances;

    bool allowingPayments = true;
    bool allowingWithdrawals = true;

    event LogContribution(address sender, uint amount,
                          address recipient1, uint amount1,
                          address recipient2, uint amount2);
    event LogWithdrawal(address beneficiary, uint amount);
    event LogAllowPaymentsChange(bool oldState, bool allowPayments);
    event LogAllowWithdrawalsChange(bool oldState, bool allowWithdrawals);
    event LogUpdateBalance(address account, uint oldBalance, uint newBalance);

    function allowPayments(bool allowed)
        public
        onlyOwner
    {
        require(allowed != allowingPayments);
        LogAllowPaymentsChange(allowingPayments, allowed);
        allowingPayments = allowed;
    }

    function acceptWithdrawals(bool allowed)
        public
        onlyOwner
    {
        require(allowed != allowingWithdrawals);
        LogAllowWithdrawalsChange(allowingWithdrawals, allowed);
        allowingWithdrawals = allowed;
    }

    function getAddressBalance(address addr)
        public
        constant
        returns (uint)
    {
        return balances[addr];
    }

    function withdrawFunds() public returns (uint amount) {
        require(allowingWithdrawals);
        address sender = msg.sender;
        amount = balances[sender];
        require(amount > 0);

        balances[sender] = 0;
        sender.transfer(amount);
        LogWithdrawal(msg.sender, amount);
        return amount;
    }

    function splitPaymentEqually(uint amount, address addr1, address addr2)
        private
        returns (uint toAddr1, uint toAddr2)
    {
        toAddr1 = amount / 2;
        toAddr2 = amount / 2;

        // If we have an odd number, we'll pick one recipient
        // to get the extra wei. We'll base this on the modulo
        // of the block number, as this is fairly random.
        if (amount % 2 == 1) {
            if (block.number % 2 == 0) {
                toAddr1 += 1;
            } else {
                toAddr2 += 1;
            }
        }

        LogUpdateBalance(addr1, balances[addr1], balances[addr1] + toAddr1);
        balances[addr1] += toAddr1;
        LogUpdateBalance(addr2, balances[addr2], balances[addr2] + toAddr2);
        balances[addr2] += toAddr2;
    }

    function splitPayment(address addr1, address addr2)
        public
        payable
        returns (bool)
    {
        require(allowingPayments);
        require(msg.value > 0);
        var (amount1, amount2) = splitPaymentEqually(msg.value, addr1, addr2);
        LogContribution(msg.sender, msg.value, addr1, amount1, addr2, amount2);
        return true;
    }

 }
