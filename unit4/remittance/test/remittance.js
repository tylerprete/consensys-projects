var Remittance = artifacts.require("./Remittance.sol");

contract('Remittance', function(accounts) {
  it("should create a Remittance", function() {
    return Remittance.deployed().then(function(instance) {
      return 10000;
    }).then(function(balance) {
      assert.equal(balance.valueOf(), 10000, "10000 wasn't in the first account");
    });
  });
});